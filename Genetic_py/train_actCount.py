#!/usr/local/bin/python3

"""
train_actCount_mehthod1.py

Method 1 - map values above a given threshold  (mapping)
Method 2 - sum values above given threhsold    (continuous model)
Method 3 - count of events above threshold     (binary)

Graeme Lyon
"""

import genetic as ga
import numpy as np
import sys
import pandas as pd
import copy
import matplotlib.pyplot as plt
import math
#from sklearn.model_selection import train_test_split

PLOT = True


def main():

    """
    Read data
    """
    df = pd.read_csv('actCount_trainData.csv', names=['corrMax', 'spearMax', 'algAct', 'actCountRf', 'subjId'])
    
    
    """
    Data pre-processing
    """
    # Select variables
    ip_features = df.values[:, 1]
    op_target   = df.values[:, -2]
    subj_ids    = df.values[:, -1]

    # Reshape per epoch
    nEpoch      = ip_features.size
    newSz       = int(nEpoch/30)
    ip_features = ip_features.reshape(newSz, 30)
    op_target   = op_target.reshape(newSz, 30)
    op_target   = op_target[:,1]
    subj_ids    = subj_ids.reshape(newSz, 30)
    subj_ids    = subj_ids[:,1]
    
    # Remove bad data
    idxIsNaN    = np.isnan(ip_features).any(axis=1) | np.isnan(op_target)
    ip_features = ip_features[~idxIsNaN, :]
    op_target   = op_target[~idxIsNaN]
    subj_ids    = subj_ids[~idxIsNaN]   
    
    # Shuffle data
    ip_features, op_target, subj_ids = unison_shuffled_copies(ip_features, op_target, subj_ids)
    
    # Remove zeros
    idxZeros       = op_target==0
    numZeros       = np.sum(idxZeros==True)
    pctZerosRemove = 0.85
    lenZero        = math.floor(numZeros*pctZerosRemove)
    np.delete(ip_features, idxZeros[0:lenZero-1])
    np.delete(op_target,   idxZeros[0:lenZero-1])
    np.delete(subj_ids,    idxZeros[0:lenZero-1])

    # Cut data size down
    pct         = 0.99
    N           = int(np.floor(pct*op_target.size))
    ip_features = ip_features[0:N,:]
    op_target   = op_target[0:N]
    subj_ids    = subj_ids[0:N]

    
    """
    Genetic algorithm
    """
    max_gen = 1000
    n_pop = 30
    gen_gap = 0.75
    mut_rate = 0.25
    mut_shrink = np.linspace(0.5, 0.001, num=max_gen)
    # Define weight boundaries -  thresh, countAdd
    field_lims = np.array([[0.1, 0.9], [0.5, 5]])
    gen = ga.Genetic(n_pop, gen_gap, mut_rate, mut_shrink, field_lims)
    gen.createPopulation()

    # Begin evolution of chroms
    for g in range(max_gen):
        # Evaluate each chrom on training dataset
        for chrom in gen.population:
            ip = copy.deepcopy(ip_features)
            train_err = check_performance(chrom.getWeights(), ip, op_target, subj_ids)
            chrom.setObjVal(train_err)

        # Calculate error for this generation
        gen.getMinObjVal()
        best_wts  = gen.getMinObjValWeights()
        ip        = copy.deepcopy(ip_features)
        train_err = check_performance(best_wts, ip, op_target, subj_ids)

        if PLOT and (gen.n_gen % 2) == 0:
            if gen.n_gen == 0:
                plt.figure(0)
                plt.ion()
                plt.ylabel('Cost')
                plt.xlabel('N generation')
                text = plt.text(0.1, 0.8, "", verticalalignment='center',
                                transform=plt.gca().transAxes, fontsize=13)
            plt.semilogy(gen.n_gen, train_err, 'xb', label='train_error')
            text.set_text("Cost\nTrain = {:0.5f}".format(train_err))
            plt.draw()
            plt.pause(0.001)
            print('Generation ' + str(gen.n_gen))
            print('%0.5f %0.5f = %0.9f' % (best_wts[0], best_wts[1], train_err))

        # Evolve our population!
        # Evaluate, select, combine and mutate chroms
        gen.calcFitness()
        gen.rouletteSelect()
        gen.crossOver()
        gen.mutate()
        gen.addChrom(best_wts)
        
        

"""
Helper functions
"""
def unison_shuffled_copies(a, b, c):
    assert len(a) == len(b)
    p = np.random.permutation(len(a))
    return a[p,:], b[p], c[p]


def check_performance(wts, f, target, subj_ids):
    f[np.where(f < wts[0])]  = 0
    f[np.where(f >= wts[0])] = wts[1]
    h = np.sum(f, 1)
    h[np.where(h>30)] = 30
    
    ids = np.unique(subj_ids)
    n = 0
    subj_err = []
    for id in ids:
        idx = subj_ids==id
        tmp_err = np.abs(np.mean(target[idx] - h[idx]))
        subj_err.append(tmp_err)
        n = n+1
    err  = np.median(subj_err)
    return err

def get_stats(y, h):
    difs = y - h
    std = np.std(difs)
    bias = np.mean(difs)
    stats = {'rmsd': math.sqrt(bias**2 + std**2), 'bias': bias, 'std': std}
    return stats

"""
Main
"""
if __name__ == "__main__":
    if (len(sys.argv) > 1):
        print('no arguments required')
        sys.exit(2)
    print('\nTraining...')
    main()
    print('Complete!\n')
    sys.exit

# EOF