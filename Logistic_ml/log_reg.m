function log_reg

% data
d = load('fisheriris');
X = d.meas;
m = size(X,1);
X = [ones(m,1), X];
targ_class = d.species;
y = zeros(m, 1);
y = y.';
y(strcmp(targ_class, 'setosa')) = 1; %select this class against others
lambda = 1;
alpha = 0.01;

% Initialize weights
Wts = zeros(size(X,2),1);

figure; hold on;

max_iter = 1e3;
for i=1:max_iter
    
    % Predict
    h = sigmoid(X*Wts);
    
    % Cross-entrpoy cost
    J = (1/m) * (y*log(h) + (1-y)*log(1-h));
    % Regularization
    reg = (lambda/(2*m))*sum(Wts(2:end).^2);
    % Total cost
    cost = J + reg;
    
    if mod(i,5)==0
        plot(i, cost, 'o');
        drawnow;
    end
    
%     % Calculate grdients and update weights
%     grad_tmp = (1/m) * (y-h.')*X;
%     H = (1/m)* (h*(1-h)*X*X');
%     reg = (lambda/m)*[0; Wts(2:end)]; % We don't regularize theta0 bias term
%     grad = grad_tmp' + reg;
%     Wts = Wts + alpha*(grad);

    % Calculating gradient and hessian.
    grad = (1/m) * (h.'-y)*X;
    H = (1/m)*diag(h)*diag(1-h)*(X*X.');
    
    % Calculate J (for testing convergence)
    J(i) =(1/m)*sum(-y.*log(h) - (1-y).*log(1-h));
    
    theta = theta - H\grad;



end

h = sigmoid(X*Wts);




function y = sigmoid(x)
y = 1 ./ (1 + exp(-x));


