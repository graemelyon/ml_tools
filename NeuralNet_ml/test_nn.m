%
% test_nn
% Graeme Lyon
%
% Simple vectorized neural network implementation for classification.
% Each backprop stage can be performed in one calculation via matrices or
% split into mini-batches to help avoid local minima.
% Uses Nesterov accelerated descent (Ref: http://arxiv.org/pdf/1212.0901v2.pdf)
% and simulated annealing to improve robustness.
% Can be eaily adapted for non-linear function mapping with different
% network structure/activation units.
%
% Test neural network system by attempting to classify data using the 
% classic ML iris flowers dataset. There are 3 classes of flower with 4
% input features descibes below.
%

function test_nn
% clear; close all;

load('iris_data.mat');
% X = input features
% y = class vector
% Data Set Information:
%    --- This is perhaps the best known database to be found in the pattern
%        recognition literature.  Fisher's paper is a classic in the field
%        and is referenced frequently to this day.  (See Duda & Hart, for
%        example.)  The data set contains 3 classes of 50 instances each,
%        where each class refers to a type of iris plant.  One class is
%        linearly separable from the other 2; the latter are NOT linearly
%        separable from each other.
%    --- Predicted attribute: class of iris plant.
% Attribute Information:
%    1. sepal length in cm
%    2. sepal width in cm
%    3. petal length in cm
%    4. petal width in cm
%    5. class:
%       1 - Iris Setosa
%       2 - Iris Versicolour
%       3 - Iris Virginica

% Normalise input data
for i=1:size(X, 2)
    X(:,i) = X(:,i) - mean(X(:,i));
    X(:,i) = X(:,i) / std(X(:,i));
end
m = size(X,1); % number of examples

% Partition data to train, x-val & test
ix_train = 1:floor(0.7*m);
ix_xval = ix_train(end)+1:floor(0.85*m);
ix_test = ix_xval(end)+1:m;

X_train = X(ix_train,:);
X_xval = X(ix_xval,:);
X_test = X(ix_test,:);
y_train = y(ix_train, :);
y_xval = y(ix_xval, :);
y_test = y(ix_test,:);

% number of examples in partitions
m_train = size(X_train, 1);
m_xval = size(X_xval, 1);

% Training parameters
N_input = size(X_train, 2);     % inputs units
N_hidden = 6;                   % hidden units
N_output = size(y_train, 2);    % output units
mtm = 0.5;                      % momentum
lambda = 0.0001;                % regularisation
numEpochs = 10e3;               % number of iterations
NAG = 1;                        % Nesterov accelerated gradient
alpha = linspace(0.005, 0.001, numEpochs); % simulated annealing
J_train = nan(numEpochs,1);
J_xval = J_train;
% mini-batches
n_batches = 3;
batch_sz = floor(m_train / n_batches);
batch_cost = nan(1, n_batches);

% Initialize weights
epsilon = 0.001;
Wt1 = (rand(N_input+1, N_hidden)*2*epsilon) - epsilon;
Wt2 = (rand(N_hidden+1, N_output)*2*epsilon) - epsilon;
dw1 = 0;
dw2 = 0;

figure; hold on; box on; ylabel('Cost'); xlabel('Iteration');
l = plot(nan,nan,'ob', nan,nan,'xr'); legend(l,'train','xval');


% Train network
for n=1:numEpochs
    
    r = randperm(m_train);
    X_train = X_train(r, :);
    y_train = y_train(r, :);
    
    % use mini batches
    for k=1:n_batches
        
        idx = 1+(k-1)*batch_sz:min(m_train, k*batch_sz);
        
        % Feedforward
        [h, a2, z2, z3] = feedForward(X_train(idx,:), Wt1, Wt2);
        
        % Cost
        wt1_tmp = Wt1(2:end,:);
        wt2_tmp = Wt2(2:end,:);
        Wts_nobias = [wt1_tmp(:); wt2_tmp(:)];
        batch_cost(k) = costFunction(y_train(idx,:), h, Wts_nobias, lambda, length(idx));
        
        % Backpropagate errors
        delta_3 = (h-y_train(idx,:)).*sigmoidPrime(z3);         %op->hid
        dJdW_2 = [ones(size(a2,1),1) a2].'*delta_3;
        delta_2 = (delta_3*Wt2(2:end,:).').*sigmoidPrime(z2);   %hid->ip
        dJdW_1 = [ones(size(X_train(idx,:),1),1) X_train(idx,:)].'*delta_2;
        % Regularize (dont penalize bias terms)
        dJdW_1(2:end,:) = dJdW_1(2:end,:) + ((lambda/m_train) * Wt1(2:end,:));
        dJdW_2(2:end,:) = dJdW_2(2:end,:) + ((lambda/m_train) * Wt2(2:end,:));

        % Weight update
        if NAG==1
            % Update with Nesterov accelerated descent (see ref)
            dw1_prev = dw1;
            dw2_prev = dw2;
            dw1 = alpha(n)*dJdW_1 + mtm*dw1;
            dw2 = alpha(n)*dJdW_2 + mtm*dw2;
            Wt1 = Wt1 - (1+mtm)*dw1 - mtm*dw1_prev;
            Wt2 = Wt2 - (1+mtm)*dw2 - mtm*dw2_prev;
        else
            % Update weights with momentum
            dw1 = alpha(n)*dJdW_1 + mtm*dw1;
            dw2 = alpha(n)*dJdW_2 + mtm*dw2;
            % update
            Wt1 = Wt1 - dw1;
            Wt2 = Wt2 - dw2;
        end
            
    end
    J_train(n) = mean(batch_cost);
    [h_xval, ~, ~, ~] = feedForward(X_xval, Wt1, Wt2);
    J_xval(n) = costFunction(y_xval, h_xval, Wts_nobias, lambda, m_xval);
    
    if mod(n, 1000)==0
        h = title(sprintf('J train = %0.4f\tJ xval = %0.4f\t\talpha = %0.3f', ...
            J_train(n), J_xval(n), alpha(n)));
        set(h, 'FontSize', 13);
        idx = n-999:n;
        plot(idx, J_train(idx), 'b');
        plot(idx, J_xval(idx), 'r');
        drawnow;
    end
end

% Get stats for each class using a one vs all approach
[h_test, ~, ~, ~] = feedForward(X_test, Wt1, Wt2);
fprintf('Test data\n');
oneVsAllStats(h_test, y_test);


function oneVsAllStats(h, target)

[~, est_class] = max(h, [], 2);
[~, real_class] = max(target, [], 2);

for c = 1:size(h, 2)
    TP = sum(est_class==c & real_class==c);
    TN = sum(est_class~=c & real_class~=c);
    FP = sum(est_class==c & real_class~=c);
    FN = sum(est_class~=c & real_class==c);
    PPV = TP / (TP + FP);
    NPV = TN / (TN + FN);
    sens = TP / (TP + FN);
    spec = TN / (TN + FP);
    F1_score = 2*((PPV * sens) / (PPV + sens));
    
    fprintf('======= Class %d =======\nN\t\t= %d\nsensitivity\t= %0.2f\nspecificity\t= %0.2f\nPPV\t\t= %0.2f\nNPV\t\t= %0.2f\nF1 score\t= %0.2f\n\n', ...
        c, sum(real_class==c), sens, spec, PPV, NPV, F1_score);
end



function [h, a2, z2, z3] = feedForward(X, Wt1, Wt2)
% ip-hid layer
z2 = [ones(size(X,1),1) X]*Wt1;
a2 = sigmoid(z2);
% hid-op layer
z3 = [ones(size(a2,1),1) a2]*Wt2;
h = sigmoid(z3);


function J = costFunction(y, h, Wts_nobias, lambda, m)
% Calculate cost
cost = -y.*log(h) - (1-y).*log(1-h);
cost = (1/m) * sum(cost(:));
% Add regularization to limit overfitting
reg = (lambda/(2*m)) * (sum(Wts_nobias(:).^2));
% Total cost
J = cost + reg;


function g = sigmoid(z)
g = 1.0 ./ (1.0 + exp(-z/2));


function g = sigmoidPrime(z)
s = sigmoid(z);
g = s.*(1-s);


function y = tanhPrime( x )
th = tanh(x);
y = 1 - th.^2;


% eof