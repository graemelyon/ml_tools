%% Calculate Gradient Magnitude and Gradient Direction
clear;
% 
%%
% Read image into workspace.
I = fspecial('gaussian', [200 100], 4);
I(:, 180:end,:) = [];
I(140:end,:) = [];
% I = zeros(200,100);
I2 = fspecial('gaussian', [20 20], 5);
I(10:29, 10:29) = I2/2;
I(10:29, 1:20) = I2;
I(20:39, 5:24) = I2/2.5;
I(20:39, 40:59) = I2/1;

I = I/6 + rand(size(I))/750;
% I = I.';



%%
k = fspecial('gaussian', [10 10], 12);
Q = conv2(I,k);
% Q = Q.';

SN = zeros(size(Q));
idxAll = 1:size(Q,2);
sz = 10;
for i=1:size(Q,1)
    for j=sz:size(Q,2)-sz
        idxSig = j-sz+1:j+sz;
        idxNoise = setdiff(idxAll, idxSig);
        SN(i,j) = sum(Q(i, idxSig)) / std(Q(i, idxNoise));
    end
end
%    SN =SN.';
SN(:, 1:9) = [];
SN(:, end-9:end) = [];
SN = sum(SN);

clf; 
colormap jet;
subplot(221);
h(1) = pcolor(I);

subplot(222);
h(2)=pcolor(Q);

subplot(223);
plot(SN);

subplot(224);
h(3)=pcolor(abs(corr(Q)));

set(h, 'edgecolor', 'none');

