# Todo:
#  Add bias's
#  Add regularization
#  new cost functions
#  handle multiple outputs - auto for nOutputClass > 2
#  Genertic class for ML lib and data management (partioning, CV)
#  Stochastic or bacth sizes                                                                        
#  Maybe...implement (maybe hessian netwons L-BFGS)

import numpy as np


class NeuralNet:

    GRAD_CHECK_THRESH = 10e-8
    GRAD_CHECK_ITER = 50
    EPSILSON = 10e-6

    def __init__(self, _maxIter=500, _nHidden=4):
        # Set hyperparameters
        self.maxIter = _maxIter
        self.nHidden = _nHidden
        self.learningRate = np.linspace(0.5, 0.01, self.maxIter)
        self.momentum = 0.5
        self.enNesterov = True  # Nesterov accelerated gradient (https://arxiv.org/pdf/1212.0901v2.pdf)
        self.cost = []
        self.gradDiff = []
        self.minGrad = []
        self.dW1 = 0
        self.dW2 = 0

    def initNet(self, _nInput, _nOutput):
        self.nInput = _nInput
        self.nOutput = _nOutput
        self.W1 = np.random.rand(self.nInput, self.nHidden)/100
        self.W2 = np.random.rand(self.nHidden, self.nOutput)/100

    def train(self, X, y):
        # Initialise network structure
        self.initNet(X.shape[1], y.shape[1])

        # Train - full batch
        for m in range(self.maxIter):
            # Train net
            yHat = self.feedforward(X)
            J = self.costFunction(y, yHat)
            self.cost.append(J)
            dJdW1, dJdW2 = self.backprop(X, y, yHat)
            self.updateWeights(dJdW1, dJdW2, self.learningRate[m])

            # Check gradient descent is operating as expected
            if not m % self.GRAD_CHECK_ITER:
                if not (self.checkGradients(X, y)):
                    print('[FAIL] Gradient checking. diff = {0}'.format(self.gradDiff[-1]))
                # print('{0} : J = {1}'.format(m, J))

    def feedforward(self, X):
        self.z2 = np.dot(X, self.W1)
        self.a2 = self.sigmoid(self.z2)
        self.z3 = np.dot(self.a2, self.W2)
        yHat = self.sigmoid(self.z3)
        return yHat

    def costFunction(self, y, yHat):
        J = 0.5 * np.sum((y - yHat)**2)
        return J

    def backprop(self, X, y, yHat):
        # NB: delta 3 depends on the cost function applied
        delta3 = np.multiply(-(y - yHat), self.sigmoidPrime(self.z3))
        dJdW2 = np.dot(self.a2.T, delta3)
        delta2 = np.dot(delta3, self.W2.T) * self.sigmoidPrime(self.z2)
        dJdW1 = np.dot(X.T, delta2)
        return dJdW1, dJdW2

    def updateWeights(self, dJdW1, dJdW2, learnRate):
        if self.enNesterov:
            dW1_prev = self.dW1
            dW2_prev = self.dW2
            self.dW1 = learnRate*dJdW1 + self.momentum*self.dW1
            self.dW2 = learnRate*dJdW2 + self.momentum*self.dW2
            self.W1 = self.W1 - (1+self.momentum)*self.dW1 - self.momentum*dW1_prev
            self.W2 = self.W2 - (1+self.momentum)*self.dW2 - self.momentum*dW2_prev
        else:
            self.dW1 = learnRate*dJdW1 + self.momentum*self.dW1
            self.dW2 = learnRate*dJdW2 + self.momentum*self.dW2
            self.W1 = self.W1 - self.dW1
            self.W2 = self.W2 - self.dW2

    def getWeights(self):
        return np.concatenate((self.W1.ravel(), self.W2.ravel()))

    def setWeights(self, weights):
        W1_start = 0
        W1_end = self.nInput * self.nHidden
        self.W1 = np.reshape(weights[W1_start:W1_end], (self.nInput, self.nHidden))
        W2_end = W1_end + self.nHidden * self.nOutput
        self.W2 = np.reshape(weights[W1_end:W2_end], (self.nHidden, self.nOutput))

    def checkGradients(self, X, y):
        # Numerical gradient
        numGrad = self.computeNumericalGradient(X, y)

        # Backprop gradient
        yHat = self.feedforward(X)
        dJdW1, dJdW2 = self.backprop(X, y, yHat)
        grad = np.concatenate((dJdW1.ravel(), dJdW2.ravel()))
        self.minGrad.append(np.min(grad))

        # Compare
        diff = np.linalg.norm(grad - numGrad) / np.linalg.norm(grad + numGrad)
        self.gradDiff.append(diff)
        if diff < self.GRAD_CHECK_THRESH:
            return True
        else:
            return False

    def computeNumericalGradient(self, X, y):
        weights = self.getWeights()
        perturb = np.zeros(weights.shape)
        numGrad = np.zeros(weights.shape)

        for p in range(len(weights)):
            # Set pertubation for this weight only
            perturb[p] = self.EPSILSON
            # Positive perturbation
            self.setWeights(weights + perturb)
            yHat = self.feedforward(X)
            Jpos = self.costFunction(y, yHat)
            # Negative perturbation
            self.setWeights(weights - perturb)
            yHat = self.feedforward(X)
            Jneg = self.costFunction(y, yHat)
            # Compute Numerical Gradient
            numGrad[p] = (Jpos - Jneg) / (2 * self.EPSILSON)
            # Reset perturbation for next iteration
            perturb[p] = 0

        # Reset weights
        self.setWeights(weights)
        return numGrad

    def sigmoid(self, z):
        return 1 / (1 + np.exp(-z))

    def sigmoidPrime(self, z):
        x = np.exp(-z)
        return (x / ((1 + x)**2))
