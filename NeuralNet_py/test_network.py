#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt
from NeuralNet import NeuralNet

#  Data
X = np.array(([3, 5], [5, 1], [10, 2], [10, 2]), dtype=float)
y = np.array(([75], [82], [93], [95]), dtype=float)

# Normalize
X = X/np.amax(X, axis=0)
y = y/100

# Add bias
X = np.hstack((X, np.ones((X.shape[0], 1), dtype=X.dtype)))

# Train a network
NN = NeuralNet()
NN.train(X, y)

h = NN.feedforward(X)


# Plotting
f, (ax1, ax2, ax3) = plt.subplots(1, 3, sharey=False)
ax1.plot(NN.cost)
ax1.set(xlabel='Iteration', ylabel='Cost', title='')
ax1.grid()	
ax2.plot(NN.gradDiff, 'r')
ax2.set(xlabel='', ylabel='norm(grad-numGrad)/norm(grad+numGrad)', title='')
ax2.grid()
ax3.plot(NN.minGrad, 'k')
ax3.set(xlabel='', ylabel='minimum gradient', title='')
ax3.grid()
plt.show(block=True)