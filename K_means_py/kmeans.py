from IPython import get_ipython
get_ipython().magic('reset -sf')

import numpy as np
import matplotlib.pyplot as plt
import copy

def checkConvergance(centroids, centroids_old, i, max_cyc):
    if (i >= max_cyc) or ((centroids_old - centroids) < 0.0001).all():
        return False
    else:
        return True

# Create some random data
data = np.random.randn(1000, 2)
sz = round(data.shape[0]/3)
data[sz:, :] = data[sz:, :] + 2
data[2*sz:, 0] = data[2*sz:, 0] + 3
m_train = data.shape[0]

# Setup k-means search parameters
n_centroids = 4
centroids = data[0:n_centroids]
old_centroids = np.empty(centroids.shape)
labels = np.empty(m_train)
color = ['r', 'g', 'b', 'y']
n = 1
max_cyc = 200

while checkConvergance(centroids, old_centroids, n, max_cyc):
    
    # Find centroid with min euclidian distance    
    for i in range(m_train):
        dist = ((data[i, :]-centroids)**2).sum(axis=1)
        labels[i] = np.argmin(dist)
    
    # Update centroids
    old_centroids = copy.copy(centroids)
    plt.cla()
    plt.ion()
    for c in range(n_centroids):       
        centroids[c, :] = data[labels==c,:].mean(axis=0)
        plt.plot(data[labels==c, 0], data[labels==c, 1], '.', c=color[c], markersize=4)
        plt.plot(centroids[c, 0], centroids[c, 1], c=color[c], marker="x", 
                 markersize=10, markeredgewidth=4)
    plt.pause(0.2)
    n += 1

# EOF
